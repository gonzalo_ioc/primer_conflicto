package ioc.xtec.cat.calculadora;

import java.util.Scanner;
			
public class Calculadora {

    public static void main(String[] args) {
    

    	Calculadora calc = new Calculadora(); 
         Scanner scanner = new Scanner(System.in);

        System.out.println("Introdueix el primer número: ");
        double num1 = scanner.nextDouble();

        System.out.println("Introdueix el segon número: ");
        double num2 = scanner.nextDouble();

        System.out.println("Suma: " + calc.sumar(num1, num2));
    }
    
    /*Cambiamos el nombre de los parámetros a número*/
    double sumar(double numero1, double numero2) {
    	return numero1 + numero2;
    }
    
}


